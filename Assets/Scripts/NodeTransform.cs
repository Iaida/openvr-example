﻿using UnityEngine;
using UnityEngine.XR;

public class NodeTransform : MonoBehaviour
{
    public Nodes Nodes;

    public XRNode NodeType;

    public Vector3 Position;
    public Quaternion Rotation;
    

    private void Update()
    {
        XRNodeState? node = Nodes.GetNodeByType(NodeType);
        if (node.HasValue)
        {
            if (node.Value.TryGetRotation(out Rotation))
                transform.localRotation = Rotation;
            

            if (node.Value.TryGetPosition(out Position))
                transform.localPosition = Position;
            
        }
    }
}
