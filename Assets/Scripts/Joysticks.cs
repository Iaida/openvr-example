﻿using UnityEngine;
using System.Linq;

public class Joysticks : MonoBehaviour
{
    public int NumJoySticksActive;
    public string[] JoyStickNames;

    public void Start()
    {
        InvokeRepeating(nameof(RefreshJoySticks), 0.0f, 1.0f);
    }

    void RefreshJoySticks()
    {
        JoyStickNames = Input.GetJoystickNames();
        NumJoySticksActive = JoyStickNames.Count(s => !string.IsNullOrEmpty(s));
    }

}
