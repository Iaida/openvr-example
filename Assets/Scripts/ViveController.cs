﻿using UnityEngine;

// https://docs.unity3d.com/Manual/OpenVRControllers.html

public class ViveController : MonoBehaviour
{
    [System.Serializable]
    public struct ViveControllerState
    {
        public enum ControllerSide { Left, Right }

        public ControllerSide Side;
        public string Prefix;
        [Space(5)]
        public bool MenuButtonPress;
        [Space(5)]
        public bool TrackpadPress;
        public bool TrackpadTouch;
        public Vector2 TrackpadPos;
        [Space(5)]
        public bool TriggerTouch;
        public float TriggerSqueeze;
        [Space(5)]
        public float GripButtonSqueeze;

        public void UpdateState()
        {

            MenuButtonPress = Input.GetButton(Prefix + "Menu Button Press");
            TrackpadPress = Input.GetButton(Prefix + "Trackpad Press");
            TrackpadTouch = Input.GetButton(Prefix + "Trackpad Touch");
            TriggerTouch = Input.GetButton(Prefix + "Trigger Touch");

            TrackpadPos = new Vector2(
                Input.GetAxis(Prefix + "Trackpad Horizontal"),
                Input.GetAxis(Prefix + "Trackpad Vertical"));

            TriggerSqueeze = Input.GetAxis(Prefix + "Trigger Squeeze");
            GripButtonSqueeze = Input.GetAxis(Prefix + "Grip Button Squeeze");
        }
    }
    [Space(5)]
    public ViveControllerState LeftController = new ViveControllerState
    {
        Side = ViveControllerState.ControllerSide.Left,
        Prefix = "Left Controller "
    };
    [Space(5)]
    public ViveControllerState RightController = new ViveControllerState
    {
        Side = ViveControllerState.ControllerSide.Right,
        Prefix = "Right Controller "
    };

    private void Update()
    {
        LeftController.UpdateState();
        RightController.UpdateState();
    }
}
