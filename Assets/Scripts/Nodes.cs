﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class Nodes : MonoBehaviour
{


    public List<XRNode> NodeTypes;


    private List<XRNodeState> NodeStates;
    void Awake()
    {
        NodeStates = new List<XRNodeState>();
    }

    void Update()
    {
        InputTracking.GetNodeStates(NodeStates);
        NodeTypes = new List<XRNode>(NodeStates.Count);
        for (int i = 0; i < NodeStates.Count; i++)
        {
            NodeTypes.Add(NodeStates[i].nodeType);
        }
    }

    public XRNodeState? GetNodeByType(XRNode type)
    {
        foreach (XRNodeState node in NodeStates)
        {
            if (node.nodeType == type)
                return node;
        }
        return null;
    }
}
