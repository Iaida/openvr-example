﻿using UnityEngine;

public class FloorGizmo : MonoBehaviour
{
    public Color CubeColor = Color.blue;
    public Vector3 CubeSize = new Vector3(2, 0.01f, 2);

    private void OnDrawGizmos()
    {
        Gizmos.color = CubeColor;
        Gizmos.DrawWireCube(transform.position, CubeSize);
    }
}
