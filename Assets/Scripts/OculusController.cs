﻿using UnityEngine;

// https://docs.unity3d.com/Manual/OculusControllers.html

// TODO: not tested with device

public class OculusController : MonoBehaviour
{
    [System.Serializable]
    public struct OculusControllerState
    {
        public enum ControllerSide { Primary, Secondary }

        public ControllerSide Side;
        public string SideIdentifier;
        [Space(5)]
        public Vector2 ThumbstickPos;
        public bool ThumbstickPress;
        public bool ThumbstickTouch;
        public float ThumbstickNearTouch;
        [Space(5)]
        public bool ThumbRestTouch;
        public float ThumbRestNearTouch;
        [Space(5)]
        public bool IndexTriggerTouch;
        public float IndexTriggerNearTouch;
        public float IndexTriggerSqueeze;
        [Space(5)]
        public float HandTriggerSqueeze;

        public void UpdateState()
        {
            ThumbstickPos = new Vector2(
                Input.GetAxis("Axis2D." + SideIdentifier + "Thumbstick Horizontal"),
                Input.GetAxis("Axis2D." + SideIdentifier + "Thumbstick Vertical"));
            ThumbstickPress = Input.GetButton("Button." + SideIdentifier + "Thumbstick Press");
            ThumbstickTouch = Input.GetButton("Button." + SideIdentifier + "Thumbstick Touch");
            ThumbstickNearTouch = Input.GetAxis("Button." + SideIdentifier + "Thumbstick Near Touch");
            
            ThumbRestTouch = Input.GetButton("Touch." + SideIdentifier + "ThumbRest Touch");
            ThumbRestNearTouch = Input.GetAxis("Touch." + SideIdentifier + "ThumbRest Near Touch");

            IndexTriggerTouch = Input.GetButton("Axis1D." + SideIdentifier + "IndexTrigger Touch");
            IndexTriggerNearTouch = Input.GetAxis("Axis1D." + SideIdentifier + "IndexTrigger Near Touch");
            IndexTriggerSqueeze = Input.GetAxis("Axis1D." + SideIdentifier + "IndexTrigger Squeeze");

            HandTriggerSqueeze = Input.GetAxis("Axis1D." + SideIdentifier + "HandTrigger Squeeze");
        }
    }


    [Space(5)]
    public bool OnePress;
    public bool OneTouch;
    [Space(5)]
    public bool TwoPress;
    public bool TwoTouch;
    [Space(5)]
    public bool ThreePress;
    public bool ThreeTouch;
    [Space(5)]
    public bool FourPress;
    public bool FourTouch;
    [Space(5)]
    public bool StartPress;
    [Space(5)]

    public OculusControllerState PrimaryController = new OculusControllerState
    {
        Side = OculusControllerState.ControllerSide.Primary,
        SideIdentifier = "Primary"
    };
    [Space(5)]
    public OculusControllerState SecondaryController = new OculusControllerState
    {
        Side = OculusControllerState.ControllerSide.Secondary,
        SideIdentifier = "Secondary"
    };


    private void Update()
    {
        PrimaryController.UpdateState();
        SecondaryController.UpdateState();

        // TODO: 1, 2, 3, 4 Button Touches not detected

        OnePress = Input.GetButton("Button.One Press");
        OneTouch = Input.GetButton("Button.One Touch");

        TwoPress = Input.GetButton("Button.Two Press");
        TwoTouch = Input.GetButton("Button.Two Touch");

        ThreePress = Input.GetButton("Button.Three Press");
        ThreeTouch = Input.GetButton("Button.Three Touch");

        FourPress = Input.GetButton("Button.Four Press");
        FourTouch = Input.GetButton("Button.Four Touch");

        StartPress = Input.GetButton("Button.Start Press");
    }
}
