﻿using System.Collections.Generic;
using UnityEngine;



[CreateAssetMenu(fileName = "InputDefinitions", menuName = "InputDefinitions", order = 1)]
public class InputDefinitions : ScriptableObject
{
    [System.Serializable]
    public struct InputDef
    {
        public enum InputType { Button = 0, Axis = 2}

        public string Name;
        public InputType Type;
        public int Id;
    }

    public List<InputDef> InputDefs;
}
