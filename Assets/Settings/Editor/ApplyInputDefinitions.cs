﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


// Based on: http://plyoung.appspot.com/blog/manipulating-input-manager-in-script.html

public class ApplyInputDefinitions : EditorWindow
{
    private static InputDefinitions ChooseInputFile()
    {
        string absolutePath = EditorUtility.OpenFilePanel(
            "Choose InputDefinitions ScriptableObject Instance",
            Application.dataPath,
            "asset");

        if (string.IsNullOrEmpty(absolutePath))
        {
            Debug.LogWarning("No file selected!");
            return null;
        }

        if (!absolutePath.StartsWith(Application.dataPath))
        {
            Debug.LogError("Selected file was not located in the project folder!");
            return null;
        }

        string relativepath = "Assets" + absolutePath.Substring(Application.dataPath.Length);

        return AssetDatabase.LoadAssetAtPath<InputDefinitions>(relativepath);
    }


    [MenuItem("VR Input/Add File To Input")]
    public static void AddToInput()
    {
        InputDefinitions InputDefinitions = ChooseInputFile();
        if (!InputDefinitions)
            return;

        List<InputDefinitions.InputDef> InputDefs = InputDefinitions.InputDefs;

        Debug.Log("Adding to input");

        Object[] assets = AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/InputManager.asset");
        SerializedObject serializedObject = new SerializedObject(assets[0]);
        SerializedProperty axesProperty = serializedObject.FindProperty("m_Axes");

        int initialSize = axesProperty.arraySize;
        axesProperty.arraySize += InputDefs.Count;
        serializedObject.ApplyModifiedProperties();


        for (int i = 0; i < InputDefs.Count; i++)
        {
            ApplyToSingleProperty(axesProperty.GetArrayElementAtIndex(initialSize + i), InputDefinitions.InputDefs[i]);
        }

        serializedObject.ApplyModifiedProperties();
    }

    private static void ApplyToSingleProperty(SerializedProperty property, InputDefinitions.InputDef inputDef)
    {
        GetChildProperty(property, "m_Name").stringValue = inputDef.Name;

        if (inputDef.Type == InputDefinitions.InputDef.InputType.Button)
        {
            GetChildProperty(property, "m_Name").stringValue = inputDef.Name;
            GetChildProperty(property, "descriptiveName").stringValue = "";
            GetChildProperty(property, "descriptiveNegativeName").stringValue = "";
            GetChildProperty(property, "negativeButton").stringValue = "";
            GetChildProperty(property, "positiveButton").stringValue = "joystick button " + inputDef.Id;
            GetChildProperty(property, "altNegativeButton").stringValue = "";
            GetChildProperty(property, "altPositiveButton").stringValue = "";
            GetChildProperty(property, "gravity").floatValue = 0;
            GetChildProperty(property, "dead").floatValue = 0;
            GetChildProperty(property, "sensitivity").floatValue = 0;
            GetChildProperty(property, "snap").boolValue = false;
            GetChildProperty(property, "invert").boolValue = false;
            GetChildProperty(property, "type").intValue = (int)InputDefinitions.InputDef.InputType.Button;
            GetChildProperty(property, "axis").intValue = 0;
            GetChildProperty(property, "joyNum").intValue = 0;

        }
        else if (inputDef.Type == InputDefinitions.InputDef.InputType.Axis)
        {
            GetChildProperty(property, "m_Name").stringValue = inputDef.Name;
            GetChildProperty(property, "descriptiveName").stringValue = "";
            GetChildProperty(property, "descriptiveNegativeName").stringValue = "";
            GetChildProperty(property, "negativeButton").stringValue = "";
            GetChildProperty(property, "positiveButton").stringValue = "";
            GetChildProperty(property, "altNegativeButton").stringValue = "";
            GetChildProperty(property, "altPositiveButton").stringValue = "";
            GetChildProperty(property, "gravity").floatValue = 0;
            GetChildProperty(property, "dead").floatValue = 0;
            GetChildProperty(property, "sensitivity").floatValue = 1;
            GetChildProperty(property, "snap").boolValue = false;
            GetChildProperty(property, "invert").boolValue = false;
            GetChildProperty(property, "type").intValue = (int)InputDefinitions.InputDef.InputType.Axis;
            GetChildProperty(property, "axis").intValue = inputDef.Id - 1;
            GetChildProperty(property, "joyNum").intValue = 0;
        }
        else
        {
            Debug.LogError("Invalid Enum!");
        }
    }

    private static SerializedProperty GetChildProperty(SerializedProperty parent, string name)
    {
        SerializedProperty child = parent.Copy();
        child.Next(true);
        do
        {
            if (child.name == name)
                return child;
        }
        while (child.Next(false));

        return null;
    }
}
